import React from 'react';

const ToDoList = ({ tasks, toggleTask, deleteTask }) => {
    return (
        <div className="task-item" key={tasks.id}>
            <div className={tasks.status ? "default-task-text" : "strike-out"} onClick={() => toggleTask(tasks.id)}>
                {tasks.id}. {tasks.description}
            </div>
            <button onClick={() => deleteTask(tasks.id)}>X</button>
        </div>
    );
};

export default ToDoList;