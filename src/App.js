import React, { useState } from 'react';
import ToDoForm from './components/ToDoForm';
import ToDoList from './components/ToDoList';
import './styles/App.css';

function App() {
  const [tasks, setTasks] = useState([
    {
      id: 1,
      description: 'task 1 u know',
      status: true
    },
    {
      id: 2,
      description: 'task 2 to complete',
      status: false
    }
  ]);

  const addTask = (userInput) => {
    if (userInput) {
      const newTask = {
        id: tasks.length ? tasks.length + 1 : 1,
        description: userInput,
        status: true
      }
      setTasks([...tasks, newTask]);
    }
    console.log(tasks);
  };

  const deleteTask = (id) => setTasks([...tasks.filter((tasks) => tasks.id !== id)]);

  const handleToggle = (id) => {
    setTasks([
      ...tasks.map((tasks) =>
        tasks.id === id ? { ...tasks, status: !tasks.status } : { ...tasks }
      )
    ])
  };

  return (
    <div className="App">
      <header>
        <h1>Список дел:</h1>
      </header>
      <ToDoForm addTask={addTask} />
      {tasks.map((tasks) => <ToDoList key={tasks.id} tasks={tasks} toggleTask={handleToggle} deleteTask={deleteTask} />)}
    </div>
  );
}

export default App;
